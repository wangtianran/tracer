from django.conf.urls import url
from django.contrib import admin
from web.view import account

from app01 import views

urlpatterns = [
    url(r'^register/$', account.register, name='register'),  # register
    url(r'^send/sms/$', account.send_sms, name='send_sms'),
]
