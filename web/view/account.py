from django.shortcuts import render, HttpResponse, redirect, reverse
from web.forms.account import RegisterModelForm, SendSmsForm
from django.http import JsonResponse


def register(request):
    form = RegisterModelForm()
    return render(request, 'register.html', {'form': form})


def send_sms(request):
    """ 发送短信 """
    form = SendSmsForm(request, data=request.GET)
    if form.is_valid():
        # 发短信
        # 写redis
        return JsonResponse({'status': True})

    return JsonResponse({'status': False, 'error': form.errors})
