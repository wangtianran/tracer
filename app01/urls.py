from django.conf.urls import url
from django.contrib import admin

from app01 import views

urlpatterns = [
    url(r'^send/sms/', views.send_sms),
    url(r'^register/', views.register, name='register'),
    url(r'^code_phone/', views.code_phone, name='code_phone'),
]
